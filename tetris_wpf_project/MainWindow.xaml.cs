﻿using System;
using System.IO;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace tetris_wpf_project
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		/* Create new "TetrisGame object */
		private TetrisGame NewGame;

		/* For pause check */
		private bool IsPaused = false;

		// private int Theme = 0;

		/* Timer object */
		private Timer tmr = new Timer(500);
		
		/* Background music */
		private MediaPlayer p0 = new MediaPlayer();

		/* Key press effect music */
		private MediaPlayer p1 = new MediaPlayer();

		public MainWindow()
		{
			/* Load UI */
			InitializeComponent();

			/* Listen to Keydown Event */
			this.KeyDown += new KeyEventHandler(MainWindow_KeyDown);

			/* New Game */
			NewGame = new TetrisGame();

			/* Timer (0.5sec Interval) */
			tmr.Elapsed += TimerHandler;
			tmr.Start();

			/* Load settings */

			// Music
			p0.Open(new System.Uri(Directory.GetCurrentDirectory() + @"\Resources\korobeiniki_remix.mid"));
			p0.MediaEnded += P0_MediaEnded;

			MusicToggler.IsChecked = Properties.Settings.Default.MusicToggleSetting;
			if (!MusicToggler.IsChecked) p0.Pause();
			else {
				p0.Play();
				p0.Volume = 0.3;
			}

			// Sound effect
			SoundEffectToggler.IsChecked = Properties.Settings.Default.SoundEffectToggleSetting;
			if (!SoundEffectToggler.IsChecked) p1.Volume = 0;
		}

		private void P0_MediaEnded(object sender, EventArgs e)
		{
			p0.Position = TimeSpan.Zero;
			p0.Play();
		}

		/// <summary>
		/// Sự kiện ấn phím
		/// </summary>
		public void MainWindow_KeyDown(object sender, KeyEventArgs e)
		{
			if (IsPaused == true) return;
			//tmr.Pause();
			lock (NewGame) {
				if (e.Key == Key.Up) {
					NewGame.TryRotatingTetromino();
				}
				if (e.Key == Key.Left) {
					NewGame.TryMovingTetromino(false);
				}
				if (e.Key == Key.Right) {
					NewGame.TryMovingTetromino(true);
				}
				if (e.Key == Key.Down) {
					NewGame.UpdateNextTick();
				}
			}
			//tmr.Resume();
			p1.Open(new System.Uri(Directory.GetCurrentDirectory() + @"\Resources\KeyPress.wav"));
			p1.Play();
			Draw();
		}

		/// <summary>
		/// Vẽ lên giao diện người dùng
		/// </summary>
		public void Draw()
		{
			this.Dispatcher.Invoke(() => {
				// Vẽ bảng
				TetrisBoard.Children.Clear();
				for (int k = 3; k < NewGame.BoardArray.GetLength(0); k++) {
					for (int l = 0; l < NewGame.BoardArray.GetLength(1); l++) {
						int val = NewGame.BoardArray[k, l];
						if (val >= 1) {
							Rectangle Block = new Rectangle() {
								Width = Double.NaN,
								Height = Double.NaN,
								Fill = new SolidColorBrush(IsPaused ? Color.FromRgb(97, 97, 97) : Tetromino.NumberToColor(val)),
								Margin = new Thickness(1)
							};
							Grid.SetColumn(Block, l);
							Grid.SetRow(Block, k - 3);
							TetrisBoard.Children.Add(Block);
						}
						else {
							Rectangle Block = new Rectangle() {
								Style = (Style)FindResource("EmptyBlock")
							};
							Grid.SetColumn(Block, l);
							Grid.SetRow(Block, k - 3);
							TetrisBoard.Children.Add(Block);
						}
					}
				}

				// Vẽ ra Tetromino kế tiếp
				Tetromino TempTetromino = (Tetromino)Activator.CreateInstance(NewGame.NextTetromino.GetType(), new sbyte[] { 0, 0 }, (sbyte)0);

				NextTetrominoGrid.Children.Clear();
				NextTetrominoGrid.RowDefinitions.Clear();
				NextTetrominoGrid.ColumnDefinitions.Clear();

				for (int i = 0; i < TempTetromino.Shape.GetLength(0); i++) {
					NextTetrominoGrid.RowDefinitions.Add(new RowDefinition() {
						Height = new GridLength(32)
					});
				}

				for (int i = 0; i < TempTetromino.Shape.GetLength(1); i++) {
					NextTetrominoGrid.ColumnDefinitions.Add(new ColumnDefinition() {
						Width = new GridLength(32)
					});
				}

				for (int k = 0; k < TempTetromino.Shape.GetLength(0); k++) {
					for (int l = 0; l < TempTetromino.Shape.GetLength(1); l++) {
						if (TempTetromino.Shape[k, l] >= 1) {
							Rectangle Block = new Rectangle() {
								Width = Double.NaN,
								Height = Double.NaN,
								Fill = new SolidColorBrush(Tetromino.NumberToColor(TempTetromino.Shape[k, l])),
								Margin = new Thickness(1)
							};
							Grid.SetColumn(Block, l);
							Grid.SetRow(Block, k);
							NextTetrominoGrid.Children.Add(Block);
						}
					}
				}

				// Hiển thị điểm và thông báo game over
				ScoreNumber.Text = NewGame.Score.ToString();
				if (NewGame.IsGameOver()) {
					tmr.Stop();
					this.KeyDown -= new KeyEventHandler(MainWindow_KeyDown);
					ScoreTextBorder.Background = new SolidColorBrush(Color.FromRgb(198, 40, 40));
					ScoreText.Text = "GAME OVER";
				}
				else {
					ScoreTextBorder.Background = new SolidColorBrush(Color.FromRgb(21, 101, 192));
					ScoreText.Text = "SCORE";
				}
			});
		}

		/// <summary>
		/// Hành động thực hiện khi mỗi 0,5s trôi qua
		/// </summary>
		private void TimerHandler(object sender, EventArgs e)
		{
			if (IsPaused == true) return;
			//tmr.Pause();
			lock (NewGame) {
				NewGame.UpdateNextTick();
			}
			//tmr.Resume();
			Draw();
		}

		/* Command logics for menubar items */

		private void NewCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
		{
			e.CanExecute = true;
		}

		private void NewCommand_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			NewGame = new TetrisGame();
			IsPaused = false;
			PauseMenu.IsChecked = false;
			tmr.Stop();
			tmr.Start();
		}

		private void ExitCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
		{
			e.CanExecute = true;
		}

		private void ExitCommand_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			Application.Current.Shutdown();
		}

		private void PauseCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
		{
			if (NewGame.IsGameOver())
				e.CanExecute = false;
			else e.CanExecute = true;
		}

		private void PauseCommand_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			if (IsPaused == false) {
				IsPaused = true;
				PauseMenu.IsChecked = true;
				Draw();
			}
			else {
				IsPaused = false;
				PauseMenu.IsChecked = false;
				Draw();
			}
		}

		/*
        private void LightTheme_CanExecute(object sender, CanExecuteRoutedEventArgs e) {
            if (Theme == 0) {
                e.CanExecute = false;
            } else {
                e.CanExecute = true;
            }
        }
		
        private void LightTheme_Executed(object sender, ExecutedRoutedEventArgs e) {

        }

        private void DarkTheme_CanExecute(object sender, CanExecuteRoutedEventArgs e) {
            if (Theme == 1) {
                e.CanExecute = false;
            } else {
                e.CanExecute = true;
            }
        }

        private void DarkTheme_Executed(object sender, ExecutedRoutedEventArgs e) {

        }
		*/

		private void MusicToggle_CanExecute(object sender, CanExecuteRoutedEventArgs e)
		{
			e.CanExecute = true;
		}

		private void MusicToggle_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			if (MusicToggler.IsChecked) {
				p0.Play();
			}
			else {
				p0.Pause();
			}
			Properties.Settings.Default.MusicToggleSetting = MusicToggler.IsChecked;
			Properties.Settings.Default.Save();
		}

		private void SoundEffectToggle_CanExecute(object sender, CanExecuteRoutedEventArgs e)
		{
			e.CanExecute = true;
		}

		private void SoundEffectToggle_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			if (SoundEffectToggler.IsChecked) {
				p1.Volume = 1;
			}
			else {
				p1.Volume = 0;
			}
			Properties.Settings.Default.SoundEffectToggleSetting = SoundEffectToggler.IsChecked;
			Properties.Settings.Default.Save();
		}
	}

	/* Define custom commands */

	public static class CustomCommands
	{
		public static readonly RoutedUICommand Exit = new RoutedUICommand
			(
				"Exit",
				"Exit",
				typeof(CustomCommands),
				new InputGestureCollection()
				{
					new KeyGesture(Key.F4, ModifierKeys.Alt)
				}
			);
		public static readonly RoutedUICommand Pause = new RoutedUICommand
			(
				"Pause",
				"Pause",
				typeof(CustomCommands),
				new InputGestureCollection()
				{
					new KeyGesture(Key.Space)
				}
			);

		public static readonly RoutedUICommand LightTheme = new RoutedUICommand
			(
				"LightTheme",
				"LightTheme",
				typeof(CustomCommands),
				new InputGestureCollection()
				{
					new KeyGesture(Key.L, ModifierKeys.Control)
				}
			);

		public static readonly RoutedUICommand DarkTheme = new RoutedUICommand
			(
				"DarkTheme",
				"DarkTheme",
				typeof(CustomCommands)
			);

		public static readonly RoutedUICommand MusicToggle = new RoutedUICommand
			(
				"MusicToggle",
				"MusicToggle",
				typeof(CustomCommands)
			);

		public static readonly RoutedUICommand SoundEffectToggle = new RoutedUICommand
			(
				"SoundEffectToggle",
				"SoundEffectToggle",
				typeof(CustomCommands)
			);
	}
}