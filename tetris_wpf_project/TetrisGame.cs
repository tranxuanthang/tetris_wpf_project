﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace tetris_wpf_project
{
	sealed class TetrisGame
	{
		/// <summary>
		/// "Cái túi" chứa các Tetromino được chuẩn bị trước.
		/// </summary>
		private Queue<Tetromino> CurrentTetrominosBag { get; set; }

		/// <summary>
		/// Khối Tetromino hiện đang rơi.
		/// </summary>
		public Tetromino CurrentTetromino { get; private set; }

		/// <summary>
		/// Khối tetromino được chọn để rơi xuống kế tiếp
		/// </summary>
		public Tetromino NextTetromino { get; private set; }

		/// <summary>
		/// Mảng bảng, gồm các khối đã hạ xuống và khối hiện đang rơi.
		/// </summary>
		public sbyte[,] BoardArray { get; private set; }

		/// <summary>
		/// Mảng "đã hạ cánh", tức bao gồm các khối Tetromino đã rơi xuống.
		/// </summary>
		public sbyte[,] LandedArray { get; private set; }

		/// <summary>
		/// Điểm số hiện tại.
		/// </summary>
		public int Score { get; private set; }

		/// <summary>
		/// Biến tạm cho biết khối mới tạo chưa di chuyển hay đang di chuyển.
		/// </summary>
		private bool CurrentTetrominoMoved { get; set; } = false;

		/// <summary>
		/// Đối tượng random.
		/// </summary>
		private Random Rnd { get; set; }

		/// <summary>
		/// Phương thức khởi tạo game mới.
		/// </summary>
		public TetrisGame()
		{
			BoardArray = new sbyte[23, 10];
			LandedArray = new sbyte[23, 10];
			Rnd = new Random();
			UpdateCurrentTetromino();
			Debug.WriteLine(CurrentTetromino.GetType());
			Score = 0;
		}

		/// <summary>
		/// Tạo ra một "túi" với 7 Tetromino khác nhau được sắp xếp ngẫu nhiên.
		/// </summary>
		/// <returns>Một hàng đợi các Tetromino.</returns>
		private Queue<Tetromino> GetRandomTetrominosBag()
		{
			List<Tetromino> Tetrominos = new List<Tetromino> {
				new LShape(new sbyte[] { 1, 4 }),
				new JShape(new sbyte[] { 1, 4 }),
				new OShape(new sbyte[] { 2, 4 }),
				new TShape(new sbyte[] { 2, 4 }),
				new SShape(new sbyte[] { 2, 4 }),
				new ZShape(new sbyte[] { 2, 4 }),
				new IShape(new sbyte[] { 0, 4 })
			};
			int RemainingUnshuffled = Tetrominos.Count;

			while (RemainingUnshuffled > 0) {
				int RandNum = Rnd.Next(RemainingUnshuffled);
				Tetromino TempTetromino = Tetrominos[RandNum];
				Tetrominos[RandNum] = Tetrominos[RemainingUnshuffled - 1];
				Tetrominos[RemainingUnshuffled - 1] = TempTetromino;
				RemainingUnshuffled--;
			}
			return new Queue<Tetromino>(Tetrominos);
		}

		/// <summary>
		/// Tạo ra một khối Tetromino ngẫu nhiên trong 7 loại Tetromino.
		/// </summary>
		private void UpdateCurrentTetromino()
		{
			Tetromino ReturningTetromino;
			if (CurrentTetrominosBag == null) {
				CurrentTetrominosBag = GetRandomTetrominosBag();
				ReturningTetromino = CurrentTetrominosBag.Dequeue();
			}
			else if (CurrentTetrominosBag.Count == 1) {
				ReturningTetromino = CurrentTetrominosBag.Dequeue();
				CurrentTetrominosBag = GetRandomTetrominosBag();
			}
			else {
				ReturningTetromino = CurrentTetrominosBag.Dequeue();
			}
			CurrentTetromino = ReturningTetromino;
			NextTetromino = CurrentTetrominosBag.Peek();
		}

		/// <summary>
		/// Phương thức kiểm tra đối tượng Tetromino có bị va chạm (vượt qua) đáy của bảng hay không.
		/// </summary>
		/// <param name="TempTetromino">Một đối tượng Tetromino</param>
		/// <returns>True (có bị va chạm) hoặc False (không bị va chạm).</returns>
		private bool CheckPotentialBottomCollision(Tetromino TempTetromino)
		{
			if (TempTetromino.Position[0] + TempTetromino.Height > BoardArray.GetLength(0)) {
				return true;
			}
			else {
				return false;
			}
		}

		/// <summary>
		/// Phương thức kiểm tra đối tượng Tetromino có bị va chạm (xuyên qua) phía trái của bảng hay không.
		/// </summary>
		/// <param name="TempTetromino">Một đối tượng Tetromino</param>
		/// <returns>True (có va chạm) hoặc False (không va chạm).</returns>
		private bool CheckPotentialLeftSideCollision(Tetromino TempTetromino)
		{
			if (TempTetromino.Position[1] < 0) {
				return true;
			}
			else {
				return false;
			}
		}

		/// <summary>
		/// Phương thức kiểm tra đối tượng Tetromino có bị va chạm (xuyên qua) phía trái của bảng hay không.
		/// </summary>
		/// <param name="TempTetromino">Một đối tượng Tetromino</param>
		/// <returns>True (có va chạm) hoặc False (không va chạm).</returns>
		private bool CheckPotentialRightSideCollision(Tetromino TempTetromino)
		{
			if (TempTetromino.Position[1] + TempTetromino.Width > BoardArray.GetLength(1)) {
				return true;
			}
			else {
				return false;
			}
		}

		/// <summary>
		/// Phương thức kiểm tra đối tượng có bị va chạm (đè lên) mảng "đã hạ cánh" hay chưa.
		/// </summary>
		/// <param name="TempTetromino">Một đối tượng Tetromino</param>
		/// <returns>True (có va chạm) hoặc False (không va chạm).</returns>
		public bool CheckPotentialLandedCollision(Tetromino TempTetromino)
		{
			for (int k = 0; k < TempTetromino.Shape.GetLength(0); k++) {
				for (int l = 0; l < TempTetromino.Shape.GetLength(1); l++) {
					if (TempTetromino.Shape[k, l] >= 1) {
						if (TempTetromino.Position[0] + k < LandedArray.GetLength(0)
							&& TempTetromino.Position[1] + l < LandedArray.GetLength(1)
							&& LandedArray[TempTetromino.Position[0] + k, TempTetromino.Position[1] + l] >= 1) {
							return true;
						}
					}
				}
			}
			return false;
		}

		/// <summary>
		/// Sát nhập khối Tetromino hiện tại với mảng "đã hạ cánh" (khi khối Tetromino đã rơi xuống và va chạm không thể rơi thêm được nữa).
		/// </summary>
		private void MergeCurrentTetrominoWithLandedArray()
		{
			sbyte[,] CurrentTetrominoArray = CurrentTetromino.Shape;
			for (int k = 0; k < CurrentTetrominoArray.GetLength(0); k++) {
				for (int l = 0; l < CurrentTetrominoArray.GetLength(1); l++) {
					if (CurrentTetromino.Shape[k, l] >= 1) {
						LandedArray[CurrentTetromino.Position[0] + k, CurrentTetromino.Position[1] + l] = (sbyte)(CurrentTetromino.Shape[k, l]);
					}
				}
			}
		}

		/// <summary>
		/// Kiểm tra game over
		/// </summary>
		/// <returns>True hoặc false</returns>
		public bool IsGameOver()
		{
			for (int i = 0; i < LandedArray.GetLength(1); i++) {
				if (LandedArray[3, i] >= 1) {
					return true;
				}
			}
			return false;
		}

		/// <summary>
		/// Cập nhật trạng thái hiện tại của game vào mảng bảng.
		/// </summary>
		private void GenerateCurrentBoardStatus()
		{
			BoardArray = new sbyte[23, 10];
			sbyte[,] CurrentTetrominoArray = CurrentTetromino.Shape;
			for (int k = 0; k < CurrentTetrominoArray.GetLength(0); k++) {
				for (int l = 0; l < CurrentTetrominoArray.GetLength(1); l++) {
					sbyte[] CurrentShapePosition = CurrentTetromino.Position;
					if (CurrentTetromino.Shape[k, l] >= 1) {
						BoardArray[CurrentShapePosition[0] + k, CurrentShapePosition[1] + l] = (sbyte)(CurrentTetromino.Shape[k, l]);
					}
				}
			}

			for (int k = 0; k < LandedArray.GetLength(0); k++) {
				for (int l = 0; l < LandedArray.GetLength(1); l++) {
					if (LandedArray[k, l] >= 1) {
						BoardArray[k, l] = LandedArray[k, l];
					}
				}
			}
		}

		/// <summary>
		/// Xoay khối Tetromino theo chiều kim đồng hồ (nếu có thể).
		/// </summary>
		public void TryRotatingTetromino()
		{
			sbyte TempShapeNumber;
			if (CurrentTetromino.ShapeNumber == 3) TempShapeNumber = 0;
			else TempShapeNumber = (sbyte)(CurrentTetromino.ShapeNumber + 1);
			Tetromino TempTetromino = (Tetromino)Activator.CreateInstance(CurrentTetromino.GetType(), new sbyte[] { CurrentTetromino.Position[0], CurrentTetromino.Position[1] }, TempShapeNumber);
			if (CheckPotentialLeftSideCollision(TempTetromino) == false && CheckPotentialRightSideCollision(TempTetromino) == false && CheckPotentialBottomCollision(TempTetromino) == false && CheckPotentialLandedCollision(TempTetromino) == false) {
				CurrentTetromino.Rotate();
				GenerateCurrentBoardStatus();
			}
		}

		/// <summary>
		/// Di chuyển khối Tetromino hiện tại qua trái hoặc phải (nếu có thể).
		/// </summary>
		/// <param name="direction">False (di chuyển sang trái) hoặc True (di chuyển sang phải).</param>
		public void TryMovingTetromino(bool direction)
		{
			if (direction == false) {
				Tetromino TempTetromino = (Tetromino)Activator.CreateInstance(CurrentTetromino.GetType(), new sbyte[] { CurrentTetromino.Position[0], (sbyte)(CurrentTetromino.Position[1] - 1) }, CurrentTetromino.ShapeNumber);
				if (CheckPotentialLeftSideCollision(TempTetromino) == false && CheckPotentialLandedCollision(TempTetromino) == false) {
					CurrentTetromino.Move(direction);
					GenerateCurrentBoardStatus();
				}
			}
			else {
				Tetromino TempTetromino = (Tetromino)Activator.CreateInstance(CurrentTetromino.GetType(), new sbyte[] { CurrentTetromino.Position[0], (sbyte)(CurrentTetromino.Position[1] + 1) }, CurrentTetromino.ShapeNumber);
				if (CheckPotentialRightSideCollision(TempTetromino) == false && CheckPotentialLandedCollision(TempTetromino) == false) {
					CurrentTetromino.Move(direction);
					GenerateCurrentBoardStatus();
				}
			}
		}

		/// <summary>
		/// Tìm các hàng có thể ăn điểm.
		/// </summary>
		/// <returns>Danh sách vị trí các hàng.</returns>
		private List<int> FindClearableRowsInLandedArray()
		{
			List<int> TempList = new List<int>();
			for (int i = 0; i < LandedArray.GetLength(0); i++) {
				bool IsClearable = true;
				for (int j = 0; j < LandedArray.GetLength(1); j++) {
					if (LandedArray[i, j] == 0) {
						IsClearable = false;
						break;
					}
				}
				if (IsClearable) {
					TempList.Add(i);
				}
			}
			return TempList;
		}

		/// <summary>
		/// Phương thức tính điểm. Nếu ăn được một lúc nhiều hàng, số điểm sẽ được cộng thêm.
		/// </summary>
		/// <param name="NumberComboOfRowsCleared">Số hàng ăn được một lúc.</param>
		/// <returns>Số điểm nhận được.</returns>
		private int CalculateScore(int NumberComboOfRowsCleared)
		{
			return (NumberComboOfRowsCleared * (NumberComboOfRowsCleared + 1)) / 2;
		}

		/// <summary>
		/// Xóa các hàng ăn được điểm khỏi bảng.
		/// </summary>
		/// <param name="ClearList">Danh sách các hàng có thể ăn điểm.</param>
		private void ClearRowsInLandedArray(List<int> ClearList)
		{
			sbyte[,] TempNewLandedArray = new sbyte[23, 10];
			int i = LandedArray.GetLength(0) - 1, j = LandedArray.GetLength(0) - 1;
			string ClOp = "";
			foreach (int item in ClearList) {
				ClOp += item + ", ";
			}
			Debug.WriteLine(ClOp);
			while (j >= 0) {

				if (!ClearList.Contains(j)) {
					for (int k = 0; k < LandedArray.GetLength(1); k++) {
						TempNewLandedArray[i, k] = LandedArray[j, k];
					}
					i--;
					j--;
				}
				else {
					j--;
				}
			}
			LandedArray = TempNewLandedArray;
			Score += CalculateScore(ClearList.Count);
		}

		/// <summary>
		/// Đọc mảng (sbyte) thành dạng chuỗi có thể đọc được.
		/// </summary>
		/// <param name="arr">Mảng có kiểu sbyte.</param>
		/// <returns>Chuỗi thông tin về mảng (sbyte).</returns>
		private static string Stringify2DArray(sbyte[,] arr)
		{
			var sb = new StringBuilder(string.Empty);
			var maxI = arr.GetLength(0);
			var maxJ = arr.GetLength(1);
			for (var i = 0; i < maxI; i++) {
				sb.Append(",\n{");
				for (var j = 0; j < maxJ; j++) {
					sb.Append($"{arr[i, j]},");
				}

				sb.Append("}");
			}

			sb.Replace(",}", "}").Remove(0, 1);
			return sb.ToString();
		}

		/// <summary>
		/// Đọc mảng (int) thành dạng chuỗi có thể đọc được.
		/// </summary>
		/// <param name="arr">Mảng có kiểu sbyte.</param>
		/// <returns>Chuỗi thông tin về mảng (int).</returns>
		private static string Stringify2DArray(int[,] arr)
		{
			var sb = new StringBuilder(string.Empty);
			var maxI = arr.GetLength(0);
			var maxJ = arr.GetLength(1);
			for (var i = 0; i < maxI; i++) {
				sb.Append(",\n{");
				for (var j = 0; j < maxJ; j++) {
					sb.Append($"{arr[i, j]},");
				}
				sb.Append("}");
			}

			sb.Replace(",}", "}").Remove(0, 1);
			return sb.ToString();
		}

		/// <summary>
		/// Cập nhật trạng thái tiếp theo của game
		/// </summary>
		public void UpdateNextTick()
		{
			Tetromino TempTetromino = (Tetromino)Activator.CreateInstance(CurrentTetromino.GetType(), new sbyte[] { (sbyte)(CurrentTetromino.Position[0] + 1), CurrentTetromino.Position[1] }, CurrentTetromino.ShapeNumber);
			bool HasBottomCollision = CheckPotentialBottomCollision(TempTetromino);
			bool HasLandedCollision = CheckPotentialLandedCollision(TempTetromino);
			if (CurrentTetrominoMoved == false) {
				CurrentTetrominoMoved = true;
			}
			else if (HasBottomCollision || HasLandedCollision) {
				if (HasBottomCollision) {
					Debug.WriteLine("Detected bottom collision!");
				}
				else {
					Debug.WriteLine("Detected landed collision!");
				}
				Debug.WriteLine("Landed Array:" + Stringify2DArray(LandedArray));
				Debug.WriteLine("Board Array:" + Stringify2DArray(BoardArray));
				Debug.WriteLine("TempTetromino Shape Array:" + Stringify2DArray(TempTetromino.Shape));
				Debug.WriteLine("TempTetromino Position: " + TempTetromino.Position[0] + " " + TempTetromino.Position[1]);
				Debug.WriteLine("TempTetromino Height: " + TempTetromino.Height + ", Board Height: " + BoardArray.GetLength(0));
				Debug.WriteLine("CurrentTetromino Shape Array:" + Stringify2DArray(CurrentTetromino.Shape));
				Debug.WriteLine("CurrentTetromino Position: " + CurrentTetromino.Position[0] + " " + CurrentTetromino.Position[1]);
				Debug.WriteLine("CurrentTetromino Height: " + CurrentTetromino.Height);
				Debug.WriteLine("");
				MergeCurrentTetrominoWithLandedArray();
				ClearRowsInLandedArray(FindClearableRowsInLandedArray());
				UpdateCurrentTetromino();
			}
			else {
				CurrentTetromino.Fall();
			}
			GenerateCurrentBoardStatus();
		}
	}
}