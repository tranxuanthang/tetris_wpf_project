﻿using System.Windows.Media;

namespace tetris_wpf_project
{
	abstract class Tetromino
	{
		/// <summary>
		/// Các hình dạng khi xoay (theo chiều kim đồng hồ).
		/// </summary>
		abstract protected sbyte[][,] Shapes { get; }

		/// <summary>
		/// Hình dạng hiện tại.
		/// </summary>
		public sbyte[,] Shape {
			get {
				return Shapes[ShapeNumber];
			}
		}

		/// <summary>
		/// Số hình dạng hiện tại, từ 0-3.
		/// </summary>
		public sbyte ShapeNumber { get; set; }

		/// <summary>
		/// Vị trí hiện tại (vị trí cột, vị trí hàng).
		/// </summary>
		public sbyte[] Position { get; set; }

		/// <summary>
		/// Chiều cao của khối.
		/// </summary>
		public int Height {
			get {
				return Shape.GetLength(0);
			}
		}

		/// <summary>
		/// Chiều rộng của khối.
		/// </summary>
		public int Width {
			get {
				return Shape.GetLength(1);
			}
		}

		/// <summary>
		/// Làm cho khối rơi xuống 1 đơn vị ô.
		/// </summary>
		public void Fall()
		{
			Position[0] = (sbyte)(Position[0] + 1);
		}

		/// <summary>
		/// Làm xoay khối.
		/// </summary>
		public void Rotate()
		{
			if (ShapeNumber == 3) ShapeNumber = 0;
			else ShapeNumber = (sbyte)(ShapeNumber + 1);
		}

		/// <summary>
		/// Di chuyển khối sang trái hoặc phải.
		/// </summary>
		/// <param name="direction">Hướng di chuyển (false: trái, true: phải).</param>
		public void Move(bool direction)
		{
			if (direction == false) {
				Position[1]--;
			}
			else {
				Position[1]++;
			}
		}

		/// <summary>
		/// Phương thức tĩnh, cho biết màu sắc tương ứng với số đầu vào.
		/// </summary>
		/// <param name="number"></param>
		/// <returns></returns>
		public static Color NumberToColor(int number)
		{
			switch (number) {
				case 1:
					return LShape.BlockColor;
				case 2:
					return JShape.BlockColor;
				case 3:
					return OShape.BlockColor;
				case 4:
					return TShape.BlockColor;
				case 5:
					return SShape.BlockColor;
				case 6:
					return ZShape.BlockColor;
				case 7:
					return IShape.BlockColor;
				default:
					return OShape.BlockColor;
			}
		}
	}

	class LShape : Tetromino
	{
		private readonly static sbyte[][,] _Shapes = new sbyte[4][,]
		{
			new sbyte[,] {
				{ 1, 0 },
				{ 1, 0 },
				{ 1, 1 }
			},
			new sbyte[,] {
				{ 1, 1, 1 },
				{ 1, 0, 0 }
			},
			new sbyte[,] {
				{ 1, 1 },
				{ 0, 1 },
				{ 0, 1 }
			},
			new sbyte[,] {
				{ 0, 0, 1 },
				{ 1, 1, 1 }
			}
		};

		protected override sbyte[][,] Shapes {
			get {
				return _Shapes;
			}
		}

		public static Color BlockColor { get; } = Color.FromRgb(255, 87, 34);

		public LShape(sbyte[] Position, sbyte ShapeNumber = 0)
		{
			this.ShapeNumber = ShapeNumber;
			this.Position = Position;
		}
	}

	class JShape : Tetromino
	{
		private readonly static sbyte[][,] _Shapes = new sbyte[4][,]
		{
			new sbyte[,] {
				{ 0, 2 },
				{ 0, 2 },
				{ 2, 2 }
			},
			new sbyte[,] {
				{ 2, 0, 0 },
				{ 2, 2, 2 }
			},
			new sbyte[,] {
				{ 2, 2 },
				{ 2, 0 },
				{ 2, 0 }
			},
			new sbyte[,] {
				{ 2, 2, 2 },
				{ 0, 0, 2 }
			}
		};

		protected override sbyte[][,] Shapes {
			get {
				return _Shapes;
			}
		}

		public static Color BlockColor { get; } = Color.FromRgb(63, 81, 181);

		public JShape(sbyte[] Position, sbyte ShapeNumber = 0)
		{
			this.ShapeNumber = ShapeNumber;
			this.Position = Position;
		}
	}

	class OShape : Tetromino
	{
		private readonly static sbyte[][,] _Shapes = new sbyte[4][,]
		{
			new sbyte[,] {
				{ 3, 3 },
				{ 3, 3 }
			},
			new sbyte[,] {
				{ 3, 3 },
				{ 3, 3 }
			},
			new sbyte[,] {
				{ 3, 3 },
				{ 3, 3 }
			},
			new sbyte[,] {
				{ 3, 3 },
				{ 3, 3 }
			}
		};

		protected override sbyte[][,] Shapes {
			get {
				return _Shapes;
			}
		}

		public static Color BlockColor { get; } = Color.FromRgb(255, 235, 59);

		public OShape(sbyte[] Position, sbyte ShapeNumber = 0)
		{
			this.ShapeNumber = ShapeNumber;
			this.Position = Position;
		}
	}

	class TShape : Tetromino
	{
		private readonly static sbyte[][,] _Shapes = new sbyte[4][,]
		{
			new sbyte[,] {
				{ 0, 4, 0 },
				{ 4, 4, 4 }
			},
			new sbyte[,] {
				{ 4, 0 },
				{ 4, 4 },
				{ 4, 0 }
			},
			new sbyte[,] {
				{ 4, 4, 4 },
				{ 0, 4, 0 }
			},
			new sbyte[,] {
				{ 0, 4 },
				{ 4, 4 },
				{ 0, 4 }
			}
		};

		protected override sbyte[][,] Shapes {
			get {
				return _Shapes;
			}
		}

		public static Color BlockColor { get; } = Color.FromRgb(156, 39, 176);

		public TShape(sbyte[] Position, sbyte ShapeNumber = 0)
		{
			this.ShapeNumber = ShapeNumber;
			this.Position = Position;
		}
	}

	class SShape : Tetromino
	{
		private readonly static sbyte[][,] _Shapes = new sbyte[4][,]
		{
			new sbyte[,] {
				{ 0, 5, 5 },
				{ 5, 5, 0 }
			},
			new sbyte[,] {
				{ 5, 0 },
				{ 5, 5 },
				{ 0, 5 }
			},
			new sbyte[,] {
				{ 0, 5, 5 },
				{ 5, 5, 0 }
			},
			new sbyte[,] {
				{ 5, 0 },
				{ 5, 5 },
				{ 0, 5 }
			}
		};

		protected override sbyte[][,] Shapes {
			get {
				return _Shapes;
			}
		}

		public static Color BlockColor { get; } = Color.FromRgb(76, 175, 80);

		public SShape(sbyte[] Position, sbyte ShapeNumber = 0)
		{
			this.ShapeNumber = ShapeNumber;
			this.Position = Position;
		}
	}

	class ZShape : Tetromino
	{
		private readonly static sbyte[][,] _Shapes = new sbyte[4][,]
		{
			new sbyte[,] {
				{ 6, 6, 0 },
				{ 0, 6, 6 }
			},
			new sbyte[,] {
				{ 0, 6 },
				{ 6, 6 },
				{ 6, 0 }
			},
			new sbyte[,] {
				{ 6, 6, 0 },
				{ 0, 6, 6 }
			},
			new sbyte[,] {
				{ 0, 6 },
				{ 6, 6 },
				{ 6, 0 }
			}
		};

		protected override sbyte[][,] Shapes {
			get {
				return _Shapes;
			}
		}

		public static Color BlockColor { get; } = Color.FromRgb(183, 28, 28);

		public ZShape(sbyte[] Position, sbyte ShapeNumber = 0)
		{
			this.ShapeNumber = ShapeNumber;
			this.Position = Position;
		}
	}

	class IShape : Tetromino
	{
		private readonly static sbyte[][,] _Shapes = new sbyte[4][,]
		{
			new sbyte[,] {
				{ 7 },
				{ 7 },
				{ 7 },
				{ 7 }
			},
			new sbyte[,] {
				{ 7, 7, 7, 7 }
			},
			new sbyte[,] {
				{ 7 },
				{ 7 },
				{ 7 },
				{ 7 }
			},
			new sbyte[,] {
				{ 7, 7, 7, 7 }
			}
		};

		protected override sbyte[][,] Shapes {
			get {
				return _Shapes;
			}
		}

		public static Color BlockColor { get; } = Color.FromRgb(0, 188, 212);

		public IShape(sbyte[] Position, sbyte ShapeNumber = 0)
		{
			this.ShapeNumber = ShapeNumber;
			this.Position = Position;
		}
	}
}
